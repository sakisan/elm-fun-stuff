module Main exposing (..)

import Html as H exposing (Html, h1, button, div, span, text)
import Html.Events exposing (onClick)
import Collage exposing (..)
import Element


-- Inspired by Mathologer's Times Tables video
-- https://www.youtube.com/watch?v=qhbuKbxJsk8


main : Program Never Model Msg
main =
    H.beginnerProgram
        { model = initialModel
        , view = view
        , update = update
        }



-- MODEL


type alias Model =
    { points : Int
    , table : Int
    }


initialModel : Model
initialModel =
    Model 10 2



-- UPDATE


type Msg
    = IncrementPoints Int
    | DecrementPoints Int
    | IncrementTable
    | DecrementTable


update : Msg -> Model -> Model
update msg model =
    case msg of
        IncrementPoints increment ->
            { model | points = model.points + increment }

        DecrementPoints increment ->
            { model | points = model.points - increment }

        IncrementTable ->
            { model | table = model.table + 1 }

        DecrementTable ->
            { model | table = model.table - 1 }



{-
   base:10 multiplier:2
   0,0
   1,2
   2,4
   3,6
   4,8
   5,0
-}


generate : Int -> Int -> List ( Int, Int )
generate multiplier base =
    List.range 0 (base - 1)
        |> List.map (\input -> ( input, product input multiplier base ))


product : Int -> Int -> Int -> Int
product input multiplier base =
    (input * multiplier) % base



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ H.text "Times tables" ]
        , div []
            [ span [] [ H.text <| "points: " ++ (toString model.points) ]
            , button [ onClick <| DecrementPoints 1 ] [ H.text "-" ]
            , button [ onClick <| IncrementPoints 1 ] [ H.text "+" ]
            , button [ onClick <| DecrementPoints 100 ] [ H.text "-100" ]
            , button [ onClick <| IncrementPoints 100 ] [ H.text "+100" ]
            ]
        , div []
            [ span [] [ H.text <| "table: " ++ (toString model.table) ]
            , button [ onClick DecrementTable ] [ H.text "-" ]
            , button [ onClick IncrementTable ] [ H.text "+" ]
            ]
        , canvasView model
        , tableView model
        ]


canvasView : Model -> Html Msg
canvasView model =
    collage
        600
        600
        (List.concat
            [ outerCircle
            , dots model
            , connectingLines model
            ]
        )
        |> Element.toHtml


radius : Float
radius =
    250


outerCircle : List Form
outerCircle =
    [ circle radius
        |> outlined defaultLine
    ]


dots : Model -> List Form
dots model =
    List.range 0 (model.points - 1)
        |> List.map
            (\input ->
                circle 5
                    |> outlined defaultLine
                    |> move (coordinatesPoint input model.points)
            )


connectingLines : Model -> List Form
connectingLines model =
    generate model.table model.points
        |> List.map
            (\( input, product ) ->
                segment
                    (coordinatesPoint input model.points)
                    (coordinatesPoint product model.points)
                    |> traced defaultLine
            )


coordinatesPoint : Int -> Int -> ( Float, Float )
coordinatesPoint input points =
    let
        ratio =
            (toFloat input) / (toFloat points)

        angle =
            ratio * pi * 2
    in
        ( 0 - radius * (cos angle)
        , radius * (sin angle)
        )


tableView : Model -> Html Msg
tableView model =
    H.table []
        (generate model.table model.points
            |> List.map
                (\( input, product ) ->
                    H.tr []
                        [ H.td [] [ H.text (toString input) ]
                        , H.td [] [ H.text (toString product) ]
                        ]
                )
        )
